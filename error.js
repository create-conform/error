/////////////////////////////////////////////////////////////////////////////////////////////
//
// error
//
//    Library for standardizing error throwing.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var ERROR_UNKNOWN = "Unknown Error";
var ERROR_UNSUPPORTED_OPERATION = "Unsupported Operation";
var ERROR_INVALID_PARAMETER = "Invalid Parameter";

var originalError = Error;

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Error Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
Error = function(name, message, innerError, data) {
    this.innerError = null;
    this.data = null;

    if (!(this instanceof Error)) {
        return new Error(name, message, innerError, data);
    }
    else if (arguments.length >= 4) {
        this.data = data;
        this.innerError = innerError instanceof Error || innerError instanceof originalError ? innerError : null;
        this.message = message;
        this.name = name;
    }
    else if (arguments.length >= 3) {
        if (innerError instanceof Error || innerError instanceof originalError) {
            this.innerError = innerError;
        }
        else {
            this.data = innerError;
        }
        this.message = message;
        this.name = name;
    }
    else if (arguments.length == 2) {
        this.name = name;
        if (message instanceof Error || message instanceof originalError) {
            this.innerError = message;
        }
        else {
            this.message = message;
        }
    }
    else {
        this.name = ERROR_UNKNOWN;
        if (name instanceof Error) {
            this.innerError = name;
        }
        else {
            this.message = name;
        }
    }

    if (this.innerError) {
        // set stack to innerError stack
        this.stack = this.innerError.stack;
    }
    else if (originalError.captureStackTrace) {
        // stack trace in V8
        originalError.captureStackTrace(this, Error);
    }
    else {
        this.stack = (new originalError).stack;
    }
};
Error.prototype = Object.create(originalError.prototype);
Error.prototype.name = "Error";
if (originalError.captureStackTrace) {
    Object.defineProperties(Error, {
        "captureStackTrace" : {
            get : function() {
                return originalError.captureStackTrace;
            },
            set : function (value) {
                originalError.captureStackTrace = value;
            }
        }
    });
    Object.defineProperties(Error, {
        "prepareStackTrace" : {
            get : function() {
                return originalError.prepareStackTrace;
            },
            set : function (value) {
                originalError.prepareStackTrace = value;
            }
        }
    });
}
if (originalError.stackTraceLimit) {
    Object.defineProperties(Error, {
        "stackTraceLimit" : {
            get : function() {
                return originalError.stackTraceLimit;
            },
            set : function (value) {
                originalError.stackTraceLimit = value;
            }
        }
    });
}


///////////////////////////////////////////////////////////////////////////////////////////// 
module.exports = Error;
module.exports.ERROR_UNKNOWN = ERROR_UNKNOWN;
module.exports.ERROR_UNSUPPORTED_OPERATION = ERROR_UNSUPPORTED_OPERATION;
module.exports.ERROR_INVALID_PARAMETER = ERROR_INVALID_PARAMETER;